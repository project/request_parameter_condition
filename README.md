## Request Parameter Condition

Tools like Views, Search API, Facets, and more rely on request
parameters to process specific types of input. But, this functionality
is often limited to whatever the tool itself provides.

One limitation we encountered was the ability to conditionally render
logic based on request parameters.

For instance, I may want to display a block conditionally if a page
does not have a query parameter "search". This use case works well
with the Block Visibility Groups module, except we need a condition
for processing these parameters from the request.

As such, this module provides a condition to process parameters found
in a request.

### Maintainers
George Anderson (geoanders)
https://www.drupal.org/u/geoanders
