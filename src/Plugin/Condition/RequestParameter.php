<?php

namespace Drupal\request_parameter_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'Request Parameter' condition.
 *
 * @Condition(
 *   id = "request_parameter",
 *   label = @Translation("Request Parameter"),
 * )
 */
class RequestParameter extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a RequestPath condition plugin.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(RequestStack $request_stack, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('request_stack'),
      $configuration,
      $plugin_id,
      $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'parameter' => '',
      'operation' => '',
      'checked_value' => '',
      'case_sensitive' => 'no',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['parameter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parameter'),
      '#default_value' => $this->configuration['parameter'],
      '#description' => $this->t("Specify the query parameter."),
    ];

    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#default_value' => $this->configuration['operation'],
      '#description' => $this->t("Specify the operation to evaluate."),
      '#options' => [
        '' => $this->t('- Select Operation -'),
        'equals' => $this->t('Equals'),
        'starts_with' => $this->t('Starts with'),
        'ends_with' => $this->t('Ends with'),
        'contains' => $this->t('Contains'),
        'exists' => $this->t('Exists'),
        'not_exists' => $this->t('Does not exist'),
      ],
    ];

    $form['checked_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $this->configuration['checked_value'],
      '#description' => $this->t("Specify the value to perform the operation."),
    ];

    $form['case_sensitive'] = [
      '#type' => 'select',
      '#title' => $this->t('Case Sensitive'),
      '#default_value' => $this->configuration['case_sensitive'],
      '#options' => [
        'no' => $this->t('No'),
        'yes' => $this->t('Yes'),
      ],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['parameter'] = $form_state->getValue('parameter');
    $this->configuration['operation'] = $form_state->getValue('operation');
    $this->configuration['checked_value'] = $form_state->getValue('checked_value');
    $this->configuration['case_sensitive'] = $form_state->getValue('case_sensitive');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('Checks the @param parameter for the "@operation" value of "@check"', [
      '@param' => $this->configuration['parameter'],
      '@operation' => mb_strtolower($this->configuration['operation']),
      '@check' => $this->configuration['checked_value'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $checked_value = $this->configuration['checked_value'];
    $request = $this->requestStack->getCurrentRequest();

    // Check if parameter is passed.
    $param_value = $request->query->get($this->configuration['parameter']);

    // If parameter does not exist and we're searching for a value, it fails.
    if (empty($param_value) && !empty($checked_value)) {
      return FALSE;
    }

    // Check case sensitivity.
    if ($this->configuration['case_sensitive'] == 'no') {
      $param_value = mb_strtolower($param_value);
      $checked_value = mb_strtolower($checked_value);
    }

    // Check by operation.
    $pass = FALSE;
    switch ($this->configuration['operation']) {
      case 'equals':
        if ($param_value === $checked_value) {
          $pass = TRUE;
        }
        break;

      case 'starts_with':
        if (substr($param_value, 0, strlen($checked_value)) === $checked_value) {
          $pass = TRUE;
        }
        break;

      case 'ends_with':
        if (substr_compare($param_value, $checked_value, strlen($param_value) - strlen($checked_value), strlen($checked_value)) === 0) {
          $pass = TRUE;
        }
        break;

      case 'contains':
        if (strpos($param_value, $checked_value) !== FALSE) {
          $pass = TRUE;
        }
        break;

      case 'exists':
        if ($param_value) {
          $pass = TRUE;
        }
        break;

      case 'not_exists':
        if (empty($param_value)) {
          $pass = TRUE;
        }
        break;
    }

    if ($this->isNegated()) {
      $pass = !$pass;
    }

    return $pass;
  }

}
